package com.chachai.swingtutorial;

import javax.swing.JButton;
import javax.swing.JFrame;

class MyFrame extends JFrame {

    JButton button; 
    public MyFrame() {
        super("First JFrame");// กำหนด title โดยเรียกจาก contructor ของแม่
        button = new JButton("Click"); 
        button.setBounds(130, 100, 100, 40); // กำหนด x, y, กว้าง, สูง
        this.setLayout(null); // เอา layout manager ออกไม่งั้นตัว button มาจะเต็มหน้า frame พอเอาออกจะมาขนาดเท่ากับที่ object กำหนดไว้
        this.add(button);
        this.setSize(400, 500); // กำหนดขนาด frame
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // ปิดframe
        // frame.setVisible(true); ก็ได้
    }
}
public class Simple2 {
    public static void main(String[] args) {
        MyFrame frame = new MyFrame();
        frame.setVisible(true); // แสดง frame ตรงนี้ก็ได้เพราะ frame ในทีนี้ก็คือJFrame 
    }
}
