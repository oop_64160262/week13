package com.chachai.swingtutorial;

import javax.swing.JButton;
import javax.swing.JFrame;

public class FirstSwingExample {
    public static void main(String[] args) {
        JFrame frame = new JFrame(); // ใส่ชื่อ title ตรงนี้เลยก็ได้
        frame.setTitle("First JFrame"); // กำหนด title แบบนี้ก็ได้
        JButton button = new JButton("Click"); // เป็น object  
        button.setBounds(130, 100, 100, 40); // กำหนด x, y, กว้าง, สูง
        frame.setLayout(null); // เอา layout manager ออกไม่งั้นตัว button มาจะเต็มหน้า frame พอเอาออกจะมาขนาดเท่ากับที่ object กำหนดไว้
        frame.add(button);
        frame.setSize(400, 500); // กำหนดขนาด frame
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // ปิดframe
        frame.setVisible(true); // แสดง frame
    }
}
