package com.chachai.swingtutorial;

import javax.swing.*;

class MyApp {
    JFrame frame;
    JButton button; 
    public MyApp() {
        frame = new JFrame(); 
        frame.setTitle("First JFrame"); // กำหนด title แบบนี้ก็ได้
        button = new JButton("Click"); 
        button.setBounds(130, 100, 100, 40); // กำหนด x, y, กว้าง, สูง
        frame.setLayout(null); // เอา layout manager ออกไม่งั้นตัว button มาจะเต็มหน้า frame พอเอาออกจะมาขนาดเท่ากับที่ object กำหนดไว้
        frame.add(button);
        frame.setSize(400, 500); // กำหนดขนาด frame
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // ปิดframe
        frame.setVisible(true); // แสดง frame
    }
}

public class Simple {
    public static void main(String[] args) {
        MyApp app = new MyApp();
    }
}
